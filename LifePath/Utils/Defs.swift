//
//  Defs.swift
//  LifePath
//
//  Created by Dmytro Naumov on 17.07.21.
//

import Foundation
import CoreLocation

/// Defined constants
enum Defs {

    /// Identifiers
    enum Identifiers {
        /// Identifier of the background image fetcher
        static var pictureFetcherBackgroundTaskIdentifier: String {
            return "com.dima.lifepath.pictureFetcher"
        }
    }

    enum LocationSettings {
        /// Defines the picture download distance trigger interval in meters
        static var triggerDistanceIstanceInterval = 100
    }

    /// Image Data
    enum ImageData {
        /// Defines image download folder name
        static var imagesFolderName = "images_downloads"
    }

    /// Flikr service
    enum Flikr {
        private static var baseURLString = "https://www.flickr.com/services/rest/"
        private static var apiKey = "e651a01d8a7f4463026baae5a5e6b057"
        private static var searchAmountPerPage = 40
        // Location accuracy (1..16): world == 1, steet == 16
        private static var accuracy = 16
        // Improve results bu using lanscape tag
        private static var tag = "landscape"
        // Geo context. 1 == indoor, 2 == outdoor
        private static var geoContext = 2

        /// Provides Flikr search URL for the give location
        /// - Parameter location: location
        /// - Returns: URL object
        static func searchURLString(location: CLLocation) -> String {
            "\(baseURLString)?method=flickr.photos.search&api_key=\(apiKey)&accuracy=\(accuracy)&tags=\(tag)&geo_context=\(geoContext)&lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&per_page=\(searchAmountPerPage)&format=json&nojsoncallback=1&extras=url_m"
        }
    }
}
