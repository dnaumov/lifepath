//
//  Utils.swift
//  LifePath
//
//  Created by Dmytro Naumov on 17.07.21.
//

import Foundation

enum Utils {

    /// Defines if app is running on the simulator
    static var isSimulator: Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        return false
        #endif
    }
}
