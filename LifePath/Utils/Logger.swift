//
//  Logger.swift
//  LifePath
//
//  Created by Dmytro Naumov on 24.07.21.
//

import Foundation

enum DLog {

    /// Prints provided message only in the debug mode
    /// - Parameter message: message to pring
    static func log(_ message: String, file: String = #fileID, line: Int = #line) {
        #if DEBUG
            print("\(file):\(line)| \(message)")
        #endif
    }
}
