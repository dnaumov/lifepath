//
//  AppDelegate.swift
//  LifePath
//
//  Created by Dmytro Naumov on 17.07.21.
//

import BackgroundTasks
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupImageFetcher()
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        DLog.log("Going background")
        scheduleImageFetcher()
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    /// Setup the image fetcher bacground task
    private func setupImageFetcher() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: Defs.Identifiers.pictureFetcherBackgroundTaskIdentifier, using: nil) { [weak self] task in
            self?.backgroundImageFetcherTriggered(task: task)
        }
    }

    /// Schedule the image fetcher background task
    private func scheduleImageFetcher() {
        guard Utils.isSimulator == false else {
            return
        }
    
        // Walking speed 5-10 km/h. Lets assume 10km/h == ~3m/s
        // Lets check if there are images to download every 20 seconds
        let request = BGAppRefreshTaskRequest(identifier: Defs.Identifiers.pictureFetcherBackgroundTaskIdentifier)
        request.earliestBeginDate = Date(timeIntervalSinceNow: 20)
        do {
            try BGTaskScheduler.shared.submit(request)
            DLog.log("BGND request registered: \(request)")
        } catch {
            DLog.log("Can't setup background image fetch task: \(error.localizedDescription)")
        }
    }

    func backgroundImageFetcherTriggered(task: BGTask) {
        // Refire background task
        scheduleImageFetcher()
        DLog.log("BGND request triggered")

        guard let task = task as? BGAppRefreshTask else {
            return
        }

        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        let downloadOperation = ImageDataManager.sharedManager.updateOperation(locations: LocationManager.sharedManager.locationStorageManager.locations)
        queue.addOperations([downloadOperation], waitUntilFinished: true)
        task.setTaskCompleted(success: true)
    }
}
