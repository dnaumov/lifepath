//
//  ImageMetadata.swift
//  LifePath
//
//  Created by Dmytro Naumov on 24.07.21.
//

import Foundation
import CoreLocation

class ImageMetadata: Equatable {

    /// Initialize instanse of metadata with given location
    /// - Parameter location: location
    init(location: CLLocation) {
        let name = String(location.hashValue)
        localPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(Defs.ImageData.imagesFolderName, isDirectory: true).appendingPathComponent(name).appendingPathExtension("jpg")
        self.location = location
    }

    let location: CLLocation
    let localPathURL: URL?
    var remoteURL: URL?
    var loaded = false
    var loading = false

    var downloadObserverBlock: (()->(Void))?

    static func == (lhs: ImageMetadata, rhs: ImageMetadata) -> Bool {
        return lhs.location.hashValue == rhs.location.hashValue
    }
}
