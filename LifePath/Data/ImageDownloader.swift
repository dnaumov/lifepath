//
//  ImageDownloadOperation.swift
//  LifePath
//
//  Created by Dmytro Naumov on 18.07.21.
//

import Foundation

/// Image downloader
class ImageDownloader: NSObject {

    typealias ImageDownloadCompletion = (Bool, Error?)->Void

    private let session: URLSession = {
        let session = URLSession(configuration: .default)
        return session
    }()

    /// Downloads images in the give session and save it in the local destination URL.
    /// Rewrites already existing images
    /// - Parameters:
    ///   - remoteURL: image remote URL
    ///   - localDestinationURL: image local destination URL
    ///   - completion: completion handler
    func downloadImage(remoteURL: URL, localDestinationURL: URL, completion: ImageDownloadCompletion?) {
        DLog.log("Starting image download: \(remoteURL.absoluteString)")

        session.downloadTask(with: remoteURL) { tmpUrl, response, error in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? -1
            guard error == nil, let tempURL = tmpUrl, statusCode/100 == 2 else {
                DLog.log("Error file download request. Code: \(statusCode) Error: \(String(describing: error))")
                completion?(false, error)
                return
            }

            var succeed = true
            var error: Error? = nil
            do {
                if FileManager.default.fileExists(atPath: localDestinationURL.path) {
                    try FileManager.default.removeItem(at: localDestinationURL)
                }

                try FileManager.default.moveItem(at: tempURL, to: localDestinationURL)

            } catch let moveError {
                succeed = false
                error = moveError
                DLog.log("Error during file move: \(moveError)")
            }

            DLog.log("Image download done: \(succeed ? "OK" : "with error")")
            DispatchQueue.global().async {
                completion?(succeed, error)
            }
        }.resume()
    }
}
