//
//  ImageDataProvider.swift
//  LifePath
//
//  Created by Dmytro Naumov on 18.07.21.
//

import Foundation
import CoreLocation

class ImageDataManager {

    static let sharedManager = ImageDataManager()
    private let imageFolderURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(Defs.ImageData.imagesFolderName, isDirectory: true)
    private let imageDownloader = ImageDownloader()

    private let operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    let localImagesFolderURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(Defs.ImageData.imagesFolderName, isDirectory: true)

    private(set) var images: [ImageMetadata] = []

    private init() {
        let locations = LocationManager.sharedManager.locationStorageManager.locations
        // Initialize image items
        locations.forEach { location in
            let imageItem = ImageMetadata(location: location)
            images.append(imageItem)
        }

        cleanup()
    }

    /// Synchronize image metadata with given locations
    /// - Parameter locations: Locations to synchronize with
    func synchronizeImages(_ locations: [CLLocation]) {
        // Images and locations are saved chronologically.
        // So find last one in the locations and handle rest of the array
        let newLocations: [CLLocation]?
        if let lastLocation = images.last?.location,
           let tailSlice = locations.split(separator: lastLocation).last {
            newLocations = Array(tailSlice)
        } else {
            newLocations = locations
        }

        newLocations?.forEach({ location in
            let imageItem = ImageMetadata(location: location)
            images.append(imageItem)
        })
    }

    /// Get the image from remote provider and saves it locally
    func downloadImagesContent() {
        // As flikr can provide same result for the sligtly changed location,
        // used images helps to choose new image for the next location
        let usedRemoteImages = images.compactMap({$0.remoteURL?.absoluteString})

        // Download images
        let imagesToLoad = images.filter({$0.loading == false && $0.loaded == false})
        var downloadOperations = [Operation]()
        imagesToLoad.forEach { imageMetadata in
            imageMetadata.loading = true
            let downloadOperation = ImageDownloadOperation(imageMetadata, downloader: imageDownloader, usedImages: usedRemoteImages)
            downloadOperations.append(downloadOperation)
        }
        operationQueue.addOperations(downloadOperations, waitUntilFinished: true)
    }

    /// Provides update block operation
    /// - Parameter locations: locations array
    /// - Returns: update operation
    func updateOperation(locations: [CLLocation]) -> Operation {
        return BlockOperation {
            self.synchronizeImages(locations)
            self.downloadImagesContent()
        }
    }

    /// Removes downloaded images and create images folder if needed
    func cleanup() {
        images = []
        guard let imageFolderURL = imageFolderURL else {
            return
        }

        do {
            if FileManager.default.fileExists(atPath: imageFolderURL.path) {
                try FileManager.default.removeItem(at: imageFolderURL)
            }
            try FileManager.default.createDirectory(at: imageFolderURL, withIntermediateDirectories: false, attributes: nil)

        } catch let error {
            DLog.log("File manager error: \(error)")
        }
    }
}
