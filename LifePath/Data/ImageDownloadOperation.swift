//
//  DownloadOperation.swift
//  LifePath
//
//  Created by Dmytro Naumov on 22.07.21.
//

import Foundation
import CoreLocation

class ImageDownloadOperation: Operation {
    private let imageMetadata: ImageMetadata
    private weak var imageDownloader: ImageDownloader?
    private let session = URLSession(configuration: .default)
    private let usedImages: [String]

    private var downloadDone = false
    private var inProgress = false

    var succeed = false
    var error: Error? = nil

    init(_ metadata: ImageMetadata, downloader: ImageDownloader, usedImages: [String]) {
        self.imageMetadata = metadata
        self.imageDownloader = downloader
        self.usedImages = usedImages
    }

    override var isFinished: Bool {
        return downloadDone
    }

    override var isExecuting: Bool {
        return inProgress
    }

    override func start() {
        willChangeValue(forKey: #keyPath(isExecuting))
        inProgress = true
        didChangeValue(forKey: #keyPath(isExecuting))

        guard !isCancelled else {
            finish(succeed: false)
            return
        }
        main()
    }

    func finish(succeed: Bool, _ error: Error? = nil) {
        guard inProgress else {
            return
        }

        willChangeValue(forKey: #keyPath(isExecuting))
        willChangeValue(forKey: #keyPath(isFinished))

        inProgress = false
        downloadDone = true
        self.imageMetadata.loading = false
        self.succeed = succeed
        self.error = error

        didChangeValue(forKey: #keyPath(isFinished))
        didChangeValue(forKey: #keyPath(isExecuting))
    }

    override func main () {
        if isCancelled {
            return
        }

        guard self.imageMetadata.loaded == false else {
            finish(succeed: true)
            return
        }

        // Check if destination URL is available
        guard let destinationURL = imageMetadata.localPathURL else {
            DLog.log("Error: local path URL unavailable")
            finish(succeed: false)
            return
        }

        // Get the image URL from Flikr
        FlikrDataProvider.imageUrlForLocation(session: session, location: imageMetadata.location, usedImages: usedImages) { [weak self] url, error in
        
            DLog.log("Got flikr image URL: \(url?.absoluteString ?? "nil")")

            guard let url = url, error == nil else {
                DLog.log("Error during image data retreival \(String(describing: error))")
                self?.finish(succeed: false, error)
                return
            }

            // Download image and save to the local file
            guard let imageDownloader = self?.imageDownloader else {
                self?.finish(succeed: false)
                return
            }

            DLog.log("Add download task for remote URL: \(url.absoluteString)")
            imageDownloader.downloadImage(remoteURL: url, localDestinationURL: destinationURL) { [weak self] success, error in
                guard success else {
                    DLog.log("Error during image download: \(url). Error: \(String(describing: error))")
                    self?.finish(succeed: false, error)
                    return
                }

                DLog.log("Image downloaded. Local URL: \(self?.imageMetadata.localPathURL?.absoluteString ?? "nil")")

                // If image is downloaded set the remote URL and loaded status
                self?.imageMetadata.remoteURL = url
                self?.imageMetadata.loaded = true
                self?.imageMetadata.downloadObserverBlock?()
                self?.finish(succeed: true)
            }
        }
    }
}
