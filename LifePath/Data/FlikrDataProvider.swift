//
//  FlikrDataProvider.swift
//  LifePath
//
//  Created by Dmytro Naumov on 19.07.21.
//

import Foundation
import CoreLocation

/// Provides Flikr data
enum FlikrDataProvider {

    /// Provides image URL for the givel location
    /// - Parameters:
    ///   - session: session to work in
    ///   - location: location
    ///   - usedImages: used images array. First image from the response which is not used will be returned
    ///   - completion: completion block
    static func imageUrlForLocation(session: URLSession, location: CLLocation, usedImages: [String], completion: ((URL?, Error?)->Void)?) {
        guard let requestURL = URL(string: Defs.Flikr.searchURLString(location: location)) else {
            DLog.log("Error. Can't build url for location: \(location)")
            completion?(nil, nil)
            return
        }

        session.dataTask(with: requestURL) { data, response, error in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? -1
            guard error == nil, let dataToParse = data, statusCode/100 == 2 else {
                DLog.log("Error flikr request. Code: \(statusCode) Error: \(String(describing: error))")
                completion?(nil, error)
                return
            }

            do {
                let imageDataContainer = try JSONDecoder().decode(FlikrImageDataContainer.self, from: dataToParse)
                if let imageURLString = imageDataContainer.photos?.photo?.first(where: {usedImages.contains($0.urlString ?? "") == false })?.urlString,
                   let resultURL = URL(string: imageURLString) {
                    DLog.log("Found image \(resultURL)")
                    completion?(resultURL, nil)
                } else {
                    DLog.log("Cant provide image URL. Please check the result data container: \(imageDataContainer)")
                    completion?(nil, nil)
                }

            } catch let err {
                DLog.log("Data parse error. Please check the model and JSON structutre: \(err.localizedDescription)")
            }
        }.resume()
    }

    private struct FlikrImageDataContainer: Codable {
        let photos: Photos?
    }

    struct Photos: Codable {
        let photo: [FlikrImageModel]?
    }

    struct FlikrImageModel: Codable {
        let urlString: String?

        enum CodingKeys: String, CodingKey {
            case urlString = "url_m"
        }
    }
}

