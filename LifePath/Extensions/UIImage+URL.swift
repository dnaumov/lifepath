//
//  UIImage+URL.swift
//  LifePath
//
//  Created by Dmytro Naumov on 24.07.21.
//

import Foundation
import UIKit

extension UIImage {

    /// Initialize UIImage from the local URL
    /// - Parameter url: local file URL
    /// - Returns: UIImage instance or `nil` if file doesn't exists
    static func fromLocalURL(url: URL?) -> UIImage? {
        guard let url = url,
              FileManager.default.fileExists(atPath: url.path),
              let imageData = try? Data(contentsOf: url),
              let result = UIImage(data: imageData) else {
            return nil
        }
        return result
    }
}
