//
//  UIAlertController+window.swift
//  LifePath
//
//  Created by Dmytro Naumov on 18.07.21.
//

import Foundation
import UIKit

extension UIAlertController {

    /// Shows alert controller on topmost view controller
    func show() {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topViewController = keyWindow?.rootViewController {
            while let presentedViewController = topViewController.presentedViewController {
                topViewController = presentedViewController
            }

            topViewController.present(self, animated: true, completion: nil)
        }
    }
}
