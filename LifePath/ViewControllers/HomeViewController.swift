//
//  HomeViewController.swift
//  LifePath
//
//  Created by Dmytro Naumov on 17.07.21.
//

import UIKit

class HomeViewController: UIViewController {
    private let homeToTripControllerSegueID = "HomeToTripControllerSegueIdentifier"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        // If vew appeared, means that user eithe exit the trip or it is app start
        cleanup()
    }

    /// Stops the location update and delete collected data
    private func cleanup() {
        LocationManager.sharedManager.cleanup()
        ImageDataManager.sharedManager.cleanup()
    }
    private func navigateToTripController() {
        performSegue(withIdentifier: homeToTripControllerSegueID, sender: self)
    }

    @IBAction private func startTrip(_ sender: Any) {
        LocationManager.sharedManager.start { [weak self] in
            if LocationManager.sharedManager.status == .active {
                DispatchQueue.main.async {
                    self?.navigateToTripController()
                }
            } else {
                DLog.log("Error during location detection initializing")
            }
        }
    }
}
