//
//  TripViewController.swift
//  LifePath
//
//  Created by Dmytro Naumov on 24.07.21.
//

import UIKit

class TripViewController: UITableViewController {

    private let updateListenerID = "tripControllerListenerIdentifier"
    private let imageCellID = "ImageCell"

    private var imageModels = [ImageMetadata]()

    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }

    private func updateData() {
        DispatchQueue.global().async { [weak self] in
            ImageDataManager.sharedManager.synchronizeImages(LocationManager.sharedManager.locationStorageManager.locations)
            ImageDataManager.sharedManager.downloadImagesContent()
            DispatchQueue.main.async {
                self?.imageModels = ImageDataManager.sharedManager.images.reversed()
                self?.tableView.reloadData()
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateData()
        addDataListener()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeDataListener()
    }

    private func addDataListener() {
        LocationManager.sharedManager.locationStorageManager.addUpdateListener(identifier: updateListenerID) { [weak self] in
            self?.updateData()
        }
    }

    private func removeDataListener() {
        LocationManager.sharedManager.locationStorageManager.removeUpdateListener(identifier: updateListenerID)
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        removeDataListener()
        LocationManager.sharedManager.stop()
    }
}

extension TripViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageModels.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: imageCellID) as? ImageTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(imageModels[indexPath.row])
        return cell
    }
}
