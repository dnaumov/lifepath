//
//  ImageTableViewCell.swift
//  LifePath
//
//  Created by Dmytro Naumov on 24.07.21.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet private weak var contentImageView: UIImageView?
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView?

    private var imageData: ImageMetadata?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        setLoading(false)
        imageView?.image = nil
        imageData?.downloadObserverBlock = nil
        imageData = nil
    }


    /// Configure the cell with provided data modell
    /// - Parameter model: Data modell
    func configure(_ model: ImageMetadata) {
        imageData = model

        setLoading(model.loaded == false)

        if model.loaded {
            imageView?.image = UIImage.fromLocalURL(url: model.localPathURL)
        } else {
            imageData?.downloadObserverBlock = { [weak self] in
                DispatchQueue.main.async {
                    self?.setLoading(false)
                    self?.imageView?.image = UIImage.fromLocalURL(url: self?.imageData?.localPathURL)
                    self?.contentView.updateConstraints()
                }
            }
        }
    }

    /// Set the cell loading state (i.e. shows/hide activity indicator)
    /// - Parameter loading: defines if cell is loading
    private func setLoading(_ loading: Bool) {
        activityIndicator?.isHidden = !loading
        if loading {
            activityIndicator?.startAnimating()
        } else {
            activityIndicator?.stopAnimating()
        }
    }

}
