//
//  LocationStorage.swift
//  LifePath
//
//  Created by Dmytro Naumov on 18.07.21.
//

import Foundation
import CoreLocation

/// In memory location storage.
/// Handles the new locations and keep them if needed
class LocationStorageManager {
    typealias LocationManagerUpdateListener = () -> Void

    private var updateListeners: [String: LocationManagerUpdateListener] = [:]
    private(set) var locations: [CLLocation] = []

    /// Handle the new location. Add it only if distance is >= that required
    /// - Parameter location: location item
    func newLocationAppeared(location: CLLocation) {
        // If locations array is empty - add the first location
        guard locations.count > 0 else {
            DLog.log("New location added: \(location)")
            addNewLocation(location)
            return
        }

        // Add the new location if it is at least defined amount of meters from the last one
        guard let lastLocation = locations.last, location.distance(from: lastLocation) >= Double(Defs.LocationSettings.triggerDistanceIstanceInterval) else {
            return
        }

        DLog.log("New location added: \(location)")
        addNewLocation(location)
    }

    private func addNewLocation(_ location: CLLocation) {
        locations.append(location)
        updateListeners.values.forEach { listenerBlock in
            listenerBlock()
        }
    }

    /// Add the update listener
    /// - Parameter listener: itemo to add
    func addUpdateListener(identifier: String, _ listener: @escaping LocationManagerUpdateListener) {
        updateListeners[identifier] = listener
    }

    func removeUpdateListener(identifier: String) {
        updateListeners.removeValue(forKey: identifier)
    }

    /// Clear the collected locations
    func cleanup() {
        updateListeners = [:]
        locations = []
    }
}
