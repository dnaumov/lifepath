//
//  LocationManager.swift
//  LifePath
//
//  Created by Dmytro Naumov on 17.07.21.
//

import Foundation
import CoreLocation
import UIKit

class LocationManager: NSObject {

    enum LocationManagerState {
        /// Receiving location updates
        case active

        /// Location updates are not received
        case inactive
    }

    /// Location manager shared instance
    static let sharedManager = LocationManager()

    /// Location storage manager. Keeps all the needed location points
    let locationStorageManager = LocationStorageManager()

    private let locationManager: CLLocationManager = CLLocationManager()

    private var locationPermissionGranted: Bool {
        let granted = locationManager.authorizationStatus == .authorizedAlways
        return granted
    }

    /// Keep the active location alert
    private var activeLocationAlert: UIAlertController?
    private var startRequestCompletion: (() -> Void)?

    private(set) var status: LocationManagerState = .inactive

    private override init() {
        super.init()
    }

    /// Starts the location monitoring
    func start(completion: (()->Void)? = nil) {

        // Make sure location manager started in the main thread
        DispatchQueue.main.async {

            self.locationManager.delegate = self

            if let completion = completion {
                self.startRequestCompletion = completion
            }

            guard self.locationPermissionGranted else {
                self.handlePermissionStatus()
                return
            }

            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.pausesLocationUpdatesAutomatically = false
            self.locationManager.startUpdatingLocation()
            self.status = .active
            self.startRequestCompletion?()
            self.startRequestCompletion = nil
        }
    }

    /// Stops the location updates
    func stop() {
        locationManager.stopUpdatingLocation()
    }

    /// Stops the location updates and clean collected locations
    func cleanup() {
        stop()
        locationStorageManager.cleanup()
    }

    private func handlePermissionStatus() {
        guard locationPermissionGranted == false else {
            // If permission granted - simply start the location update
            start()
            return
        }

        if (locationManager.authorizationStatus == .notDetermined) {
            // Request permission. Delegate method will notify if permission granted
            locationManager.requestAlwaysAuthorization()
        } else {
            showLocationRequestAlert()
        }
    }

    private func showLocationRequestAlert() {
        guard activeLocationAlert == nil else {
            // Do not show alert if one is already shown
            return
        }

        let alertTitle = "Location permission missing"
        let alertMessage = "Location permission is required for the app functionality. Please allow location acceess in the user settings"
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        let calcelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] _ in
            // Call the active completion, so sender can act
            self?.startRequestCompletion?()
            self?.startRequestCompletion = nil
            self?.activeLocationAlert = nil
        }
        let openSettingsAction = UIAlertAction(title: "Allow access", style: .default) { [weak self] _ in
            self?.activeLocationAlert = nil
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                assertionFailure("Settings URL is not correct")
                return
            }
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
        alert.addAction(calcelAction)
        alert.addAction(openSettingsAction)
        activeLocationAlert = alert
        alert.show()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DLog.log("Location failed. Error: \(error)")
    }

    public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        handlePermissionStatus()
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.last {
            locationStorageManager.newLocationAppeared(location: newLocation)
        }
    }
}
