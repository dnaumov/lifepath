//
//  LifePathTests.swift
//  LifePathTests
//
//  Created by Dmytro Naumov on 17.07.21.
//

import XCTest
@testable import LifePath
import CoreLocation

class LifePathTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testImageMetadataDuplication() throws {
        let location = CLLocation(latitude: 48.132989, longitude: 11.598053)
        let imageData = ImageMetadata(location: location)
        let sameImageData = ImageMetadata(location: location)
        let differentImageData = ImageMetadata(location: CLLocation(latitude: 48.276581, longitude: 12.377085))

        XCTAssertEqual(imageData, sameImageData)
        XCTAssertNotEqual(imageData, differentImageData)
    }

    func testLocationStorageManager() {
        let storageManager = LocationStorageManager()
        let startLocation = CLLocation(latitude: 48.177872, longitude: 11.981773)
        let closeLocation = CLLocation(latitude: 48.177853, longitude: 11.981850)
        let distantLocation = CLLocation(latitude: 45.421034, longitude: 9.019991)

        XCTAssertEqual(storageManager.locations.count, 0, "Locations should be empty")
        storageManager.newLocationAppeared(location: startLocation)
        XCTAssertEqual(storageManager.locations.count, 1, "Locations should contain start location")
        storageManager.newLocationAppeared(location: closeLocation)
        XCTAssertEqual(storageManager.locations.count, 1, "Locations should contain only start location")
        storageManager.newLocationAppeared(location: distantLocation)
        XCTAssertEqual(storageManager.locations.count, 2, "Locations should contain start and distant location")
    }

}
